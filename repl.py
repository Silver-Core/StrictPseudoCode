from lexer.lexer import lg

tk = lg.build()

while True:
    statement = input('>>>')
    tokens = tk.lex(statement)
    print(f"Tokens: {list(tokens)}")
