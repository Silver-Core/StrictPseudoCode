# Why the hell would you make a Pseudocode interpreter?
I really don't know why either.
I guess it'd be useful for people who can't dry run Pseudocode programs yet.
Also see "BUT WHY?".

# But why PSEUDOCODE?
Technically, It's not Pseudocode, It's a strict subset(?) of Pseudocode.
[used by CIE](http://www.cambridgeinternational.org/images/202629-2017-2019-syllabus.pdf).
While CIE doesn't have an official language spec of sorts, We can pretty much infer the syntax
from snippets scattered across the syllabus and previous exams.
It's not consistent, But we make do with what we can get.
Though, Snippets from recent exams are favored over older ones.
But we haven't found any conflicting snippets...yet.

# But WHY?
Programming Language Design is a topic in 'A'-Level Computer Science, As such
I think it would be a great learning exercise for me and a couple of contributing friends to
learn from this.
It would also teach us to appreciate languages like Python, Scala, Kotlin, .etc for how flexible and readable the languages are. I could only imagine how nightmarish the Lexers and Parsers look.

# Can I contribute?
Absolutely, I have no idea what I'm doing.

# What's actually been done?
[X] Added Basic types.
[]  Variables.
[]  Added all Keywords and Tokens.
[]  Functions and Procedures.
[]  Pseudo-standard library.
...
[]  Optimization (Not a big focus).
