from rply import LexerGenerator

# TODO: Add Variables
lg = LexerGenerator()

# Ignores
# Whitespace in SPC is insignificant.
lg.ignore(r'\s+')

# Literals
lg.add('REAL',     r'-?\d+.\d+')
lg.add('INTEGER',  r'-?\d+')
lg.add('CHAR',     r'(\'.{1}\')')
lg.add('STRING',   r'(\".*\")')
lg.add('BOOLEAN',  r'TRUE(?!\w)|FALSE(?!\w)')

# Keywords [and Declaritives]
lg.add('DECLARE',    r'(DECLARE)(?!\w)')
lg.add('ASSIGNMENT', r'<-')
lg.add('IDENTIFIER', r'[a-zA-Z10-9_]*\w')
# lg.add('OUTPUT', )

# Symbols & Operators

lg.add('OPEN_PARENS',     r'\(')
lg.add('CLOSE_PARENS',    r'\)')

lg.add('PLUS',  r'\+')  # INTEGER + INTEGER
lg.add('MINUS', r'-')   # INTEGER - INTEGER
lg.add('DIV',   r'\/')  # INTEGER / INTEGER
lg.add('MUL',  r'\*')   # INTEGER * INTEGER
